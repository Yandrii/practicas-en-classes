package com.luigivera.cuartoa;

import android.app.Dialog;
import android.app.Notification;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Actividad_principal extends AppCompatActivity {
    Button buscar;
    Button login;
    Button registrar;
    Button PasarParametro;
    Button Fragmentos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        buscar = (Button)findViewById(R.id.button3);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent buscar =new Intent(Actividad_principal.this, actividad_buscar.class);
                startActivity(buscar);
            }
        });

        login = (Button)findViewById(R.id.button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login =new Intent(Actividad_principal.this, actividad_login.class);
                startActivity(login);
            }
        });

        registrar = (Button)findViewById(R.id.button2);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(Actividad_principal.this, actividad_registrar.class);
                startActivity(registrar);
            }
        });
        PasarParametro = (Button)findViewById(R.id.button7);

        PasarParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(Actividad_principal.this, actividad_pasar_parametro.class);
                startActivity(registrar);
            }
        });
        Fragmentos = (Button)findViewById(R.id.button8);

        Fragmentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar =new Intent(Actividad_principal.this, fragmentos.class);
                startActivity(registrar);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(Actividad_principal.this);
                dialogLogin.setContentView(R.layout.dlg_login);
                Button botonAutenticar = (Button) dialogLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(Actividad_principal.this, cajaUsuario.getText().toString()+ " " + cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogLogin.show();
                //intent = new Intent(MainActivity.this, login.class);
                //startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }
}
